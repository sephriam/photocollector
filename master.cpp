#include "master.h"

#include <httprequesthandler.h>

namespace {
const std::string RASPBERRY_ADDRESS = "172.24.1.1";//"localhost"; //"172.24.1.1";
constexpr size_t RASPBERRY_IMAGE_PORT = 56000;
constexpr size_t RASPBERRY_STEER_PORT = 8000;
}

Master::Master()
{
    imageThread = std::thread(&Master::imageLoop, this);
    steerThread = std::thread(&Master::steerLoop, this);
    saveThread = std::thread(&Master::saveLoop, this);
}

Master::~Master()
{
    stop = true;
    steerThread.join();
    imageThread.join();
    saveThread.join();
}

void Master::steer(Master::Direction d)
{
    direction = (char)d;
}

void Master::setSavePath(std::string path)
{
    std::unique_lock<std::shared_mutex> lock(saveMux);
    savePath = std::move(path);
}

void Master::setSaveInterval(size_t interval)
{
    std::unique_lock<std::shared_mutex> lock(saveMux);
    saveInterval = interval;
}

size_t Master::getSavedCounter() const
{
    return savedCounter;
}

QImage Master::getImage() const
{
    std::shared_lock<std::shared_mutex> lock(imageMux);
    return image;
}

void Master::steerLoop()
{
    HttpRequestHandler handler(RASPBERRY_ADDRESS, RASPBERRY_STEER_PORT);
    while(!stop) {
        if(direction != -1) {

            try {
                switch (direction) {
                case Forward:
                    handler.get("motors/-1/-1");
                    break;
                case Backward:
                    handler.get("motors/1/1");
                    break;
                case Left:
                    handler.get("motors/1/-1");
                    break;
                case Right:
                    handler.get("motors/-1/1");
                    break;
                default:
                    break;
                }
            } catch (...) {

            }
            direction = -1;
            {
                std::unique_lock<std::shared_mutex> lock(saveMux);
                lastMovement = std::chrono::steady_clock::now();
            }
        }
    }
}

void Master::imageLoop()
{
    HttpRequestHandler handler(RASPBERRY_ADDRESS, RASPBERRY_IMAGE_PORT);
    std::string data;
    QMatrix rm;
    rm.rotate(180);
    while(!stop) {
        try {
            data = handler.get("jpeg");
        } catch(...) {

        }
        {
            std::unique_lock<std::shared_mutex> lock(imageMux);
            image = QImage::fromData((unsigned char*)data.c_str(), data.length(), "JPEG");
            image = image.transformed(rm);
        }
    }
}

void Master::saveLoop()
{
    auto prevLastMovement = lastMovement;
    size_t interval = saveInterval;
    bool save = false;
    std::string path = savePath;
    while(!stop) {
        {
            std::shared_lock<std::shared_mutex> lock(saveMux);
            if(lastMovement != prevLastMovement) {
                prevLastMovement = lastMovement;
                save = true;
                path = savePath;
            }
            interval = saveInterval;
        }

        if(save) {
            save = !save;

            if(path.back() != '/') {
                path += '/';
            }

            saveImage(path + "image" + std::to_string(savedCounter) + ".jpeg");
            ++savedCounter;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    }
}

void Master::saveImage(const std::string &path)
{
    QImage img = getImage();
    img.save(path.c_str());
}
