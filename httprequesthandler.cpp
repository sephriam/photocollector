#include "httprequesthandler.h"

#include <stdexcept>

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

HttpRequestHandler::HttpRequestHandler(const std::string& host, unsigned short port)
    :
    _serverAddress(host),
    _serverPort(port),
    _curl(nullptr)
{
    curl_global_init(CURL_GLOBAL_DEFAULT);
}

HttpRequestHandler::~HttpRequestHandler()
{
    curl_global_cleanup();
}

void HttpRequestHandler::setServerAddress(const std::string& serverAddress)
{
    _serverAddress = serverAddress;
}

void HttpRequestHandler::setServerPort(unsigned short serverPort)
{
    _serverPort = serverPort;
}

const std::string& HttpRequestHandler::getServerAddress()
{
    return _serverAddress;
}

unsigned short HttpRequestHandler::getServerPort()
{
    return _serverPort;
}

std::string HttpRequestHandler::get(const std::string& path)
{
    _curl = curl_easy_init();
    if(!_curl) {
        throw std::runtime_error("curl is uninitialized");
    }

    curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    CURLcode res;
    std::string ret;
    std::string URL = _serverAddress + ":" + std::to_string(_serverPort) + "/" + path;
    curl_easy_setopt(_curl, CURLOPT_URL, URL.c_str());
    curl_easy_setopt(_curl, CURLOPT_WRITEDATA, &ret);
    res = curl_easy_perform(_curl);

    if(res != CURLE_OK) {
        throw std::runtime_error(curl_easy_strerror(res));
    }

    curl_easy_cleanup(_curl);

    return std::move(ret);
}

std::string HttpRequestHandler::post(const std::string& path, const std::string& params)
{
    _curl = curl_easy_init();
    if(!_curl) {
        throw std::runtime_error("curl is uninitialized");
    }
    curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    CURLcode res;
    std::string ret;
    std::string URL = _serverAddress + ":" + std::to_string(_serverPort) + "/" + path;
    curl_easy_setopt(_curl, CURLOPT_URL, URL.c_str());
    curl_easy_setopt(_curl, CURLOPT_POSTFIELDS, params.c_str());
    curl_easy_setopt(_curl, CURLOPT_WRITEDATA, &ret);
    res = curl_easy_perform(_curl);

    if(res != CURLE_OK) {
        throw std::runtime_error(curl_easy_strerror(res));
    }

    curl_easy_cleanup(_curl);

    return std::move(ret);
}
