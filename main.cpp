#include "mainwindow.h"
#include <QApplication>
#include <master.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Master m;
    MainWindow w(&m);
    w.show();

    return a.exec();
}
