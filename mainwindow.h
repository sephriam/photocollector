#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class Master;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Master* master, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void directorySelectButtonClicked();
    void updateCamera();
    void intervalBarChanged(int value);

protected:
    virtual bool eventFilter(QObject *, QEvent *e ) override;

private:
    Master* m_master;
    Ui::MainWindow *ui;
    QPolygonF m_up;
    QPolygonF m_down;
    QPolygonF m_left;
    QPolygonF m_right;
    QTimer m_timer;
};

#endif // MAINWINDOW_H
