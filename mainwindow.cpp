#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QKeyEvent>
#include <master.h>

MainWindow::MainWindow(Master* master, QWidget *parent) :
    QMainWindow(parent),
    m_master(master),
    ui(new Ui::MainWindow),
    m_up({QPointF(0,-20), QPointF(30,0), QPointF(10,0), QPointF(10,20),
         QPointF(-10,20), QPointF(-10,0), QPointF(-30,0), QPointF(0,-20)}),
    m_down({QPointF(0,20), QPointF(30,0), QPointF(10,0), QPointF(10,-20),
           QPointF(-10,-20), QPointF(-10,0), QPointF(-30,0), QPointF(0,20)}),
    m_left({QPointF(-20,0), QPointF(0,30), QPointF(0,10), QPointF(20,10),
           QPointF(20,-10), QPointF(0,-10), QPointF(0,-30), QPointF(-20,0)}),
    m_right({QPointF(20,0), QPointF(0,30), QPointF(0,10), QPointF(-20,10),
            QPointF(-20,-10), QPointF(0,-10), QPointF(0,-30), QPointF(20,0)})
{
    ui->setupUi(this);
    this->installEventFilter(this);
    connect(ui->directorySelectButton, &QPushButton::clicked, this, &MainWindow::directorySelectButtonClicked);
    ui->directionGraphicsView->setScene(new QGraphicsScene());
    ui->camGraphicsView->setScene(new QGraphicsScene());

    connect(&m_timer, &QTimer::timeout, this, &MainWindow::updateCamera);
    m_timer.start(30);

    connect(ui->intervalSlideBar, &QSlider::valueChanged, this, &MainWindow::intervalBarChanged);
    ui->directorySelectLineEdit->setText(QDir::currentPath() + "/images");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::directorySelectButtonClicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    QDir::currentPath(),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    ui->directorySelectLineEdit->setText(dir);
    m_master->setSavePath(dir.toStdString());
}

void MainWindow::updateCamera()
{
    ui->camGraphicsView->scene()->clear();
    ui->camGraphicsView->scene()->addPixmap(QPixmap::fromImage(m_master->getImage()));
}

void MainWindow::intervalBarChanged(int value)
{
    ui->intervalLabel->setText(QString::number(value * 10) + "ms");
    m_master->setSaveInterval(value*10);
}

bool MainWindow::eventFilter(QObject *, QEvent *e)
{
    if ( e->type() == QEvent::KeyPress ) {
        ui->counterLabel->setText(QString::number(m_master->getSavedCounter()));
        QKeyEvent *k = (QKeyEvent *)e;
        ui->directionGraphicsView->scene()->clear();
        switch (k->key()) {
        case Qt::Key_W:
            ui->directionGraphicsView->scene()->addPolygon(m_up,
                                                           QPen(Qt::black),
                                                           QBrush(Qt::black));
            m_master->steer(Master::Forward);
            break;
        case Qt::Key_A:
            ui->directionGraphicsView->scene()->addPolygon(m_left,
                                                           QPen(Qt::black),
                                                           QBrush(Qt::black));
            m_master->steer(Master::Left);
            break;
        case Qt::Key_S:
            ui->directionGraphicsView->scene()->addPolygon(m_down,
                                                           QPen(Qt::black),
                                                           QBrush(Qt::black));
            m_master->steer(Master::Backward);
            break;
        case Qt::Key_D:
            ui->directionGraphicsView->scene()->addPolygon(m_right,
                                                           QPen(Qt::black),
                                                           QBrush(Qt::black));
            m_master->steer(Master::Right);
            break;
        default:
            break;
        }
    } else if( e->type() == QEvent::KeyRelease ) {
        ui->directionGraphicsView->scene()->clear();
    }

    return false;
}


