#ifndef MASTER_H
#define MASTER_H

#include <atomic>
#include <thread>
#include <shared_mutex>
#include <QImage>
#include <QDir>

class Master
{
public:
    Master();
    ~Master();

    enum Direction {
        Forward,
        Backward,
        Left,
        Right
    };

    void steer(Direction);
    void setSavePath(std::string);
    void setSaveInterval(size_t);
    QImage getImage() const;
    size_t getSavedCounter() const;

private:
    std::atomic<bool> stop = false;

    std::atomic<char> direction = -1;
    mutable std::shared_mutex imageMux;
    mutable std::shared_mutex saveMux;
    QImage image;
    std::string savePath = QDir::currentPath().toStdString() + "/images";
    size_t saveInterval = 200;
    std::chrono::time_point<std::chrono::steady_clock> lastMovement;
    std::atomic<size_t> savedCounter = 0;

    std::thread steerThread;
    std::thread imageThread;
    std::thread saveThread;

    void steerLoop();
    void imageLoop();
    void saveLoop();

    void saveImage(const std::string& path);
};

#endif // MASTER_H
