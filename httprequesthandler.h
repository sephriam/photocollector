#ifndef REQUEST_HANDLER_H
#define REQUEST_HANDLER_H

#include <string>
#include <curl/curl.h>

class HttpRequestHandler
{
public:
    HttpRequestHandler() = default;
    HttpRequestHandler(const HttpRequestHandler&) = default;
    HttpRequestHandler(HttpRequestHandler&&) = default;
    ~HttpRequestHandler();
    HttpRequestHandler& operator = (const HttpRequestHandler&) = default;
    HttpRequestHandler& operator = (HttpRequestHandler&&) = default;
    HttpRequestHandler(const std::string& host, unsigned short port);

    void setServerAddress(const std::string&);
    void setServerPort(unsigned short);

    const std::string& getServerAddress();
    unsigned short getServerPort();

    std::string get(const std::string& path);
    std::string post(const std::string& path, const std::string& params);

private:
    std::string _serverAddress;
    unsigned short _serverPort;
    CURL* _curl;

};

#endif // REQUEST_HANDLER_H
